package data;

public enum Months {
    JANUARY("jan", "winter"),
    FEBRUARY("feb", "winter"),
    MARCH("mar", "spring"),
    APRIL("apr", "spring"),
    MAY("may", "spring"),
    JUNE("jun", "summer"),
    JULY("jul", "summer"),
    AUGUST("aug", "summer"),
    SEPTEMBER("sep", "autumn"),
    OCTOBER("oct", "autumn"),
    NOVEMBER("nov", "autumn"),
    DECEMBER("dec", "winter"),
    ;

    private String name;
    private String season;

    Months(String name, String season) {
        this.name = name;
        this.season = season;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }
}