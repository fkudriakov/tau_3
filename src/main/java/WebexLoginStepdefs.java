import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import utils.PropertyLoader;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class WebexLoginStepdefs {
    private String webexUrl = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag";

    private static String errorMessage = "Enter a valid email address. Example: name@email.com";
    private static String incorrectEMail = "123";
    //    private static String incorrectPassword = "Qwert123";
    private static String loginButtonId = "guest_signin_button";
    private static String emailTextInputXpath = "//input[@class='md-input']";
    private static String okButtonId = "IDButton2";
    private static String okButtonCss = "#IDButton2";
    private static String idErrorMessage = "nameContextualError1";

    private static String myPortalElemXpath = "//div[@class='idpDescription float']/span[text()='MyPortal Account (Alternative)']";
    private static String myPortalUsernameXpath = "//input[@id='username']";
    private static String myPortalPasswordXpath = "//input[@id='password']";
    private static String myPortalLoginButtonLocator = "#submit";
    private static String urlForLoggedUser = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag&from_login=true";
    private static String userDropdownXpath = "//a[@class='avatar_name']";
    private static String signoutButtonXpath = "//a[text()='Sign out']";

    private static String correctEMail = PropertyLoader.loadProperty("email");
    private static String correctPassword = PropertyLoader.loadProperty("pass");

    @Given("Webex start page is opened")
    public void webexStartPageIsOpened() {
        open(webexUrl);
    }

    @When("Data of sign in page is submitted")
    public void dataOfSignInPageIsSubmitted() {
        $(By.id(loginButtonId)).click();
        $(By.xpath(emailTextInputXpath)).val(correctEMail);
        $(okButtonCss).click();
        $$(By.id(idErrorMessage)).shouldHaveSize(0);
    }

    @And("Login data of portal is submitted")
    public void loginDataOfPortalIsSubmitted() {
        $(By.xpath("//span[contains(text(),'MyPortal Account')]/ancestor::div[contains(@class, 'idpDescription float')]")).click();

        $(By.name("userId")).val(correctEMail);
        $(By.id("password")).val(correctPassword);
        $(By.cssSelector(myPortalLoginButtonLocator)).click();

    }

    @Then("User is logged successful")
    public void userIsLoggedSuccessful() {
        $("#_loginForm_ > div.info-box.error.offset-top-3").should(not(exist));
        $$(By.xpath("//*[@class='pmr_card_c_title']/h1")).texts().contains("Personal Room");
    }

    @And("User logout")
    public void userLogout() {
        $(By.xpath(userDropdownXpath)).click();
        $(By.xpath(signoutButtonXpath)).click();
        closeWindow();
    }

    @When("Login data is incorrect")
    public void loginDataIsIncorrect() {
        $(By.id(loginButtonId)).click();
        $(By.xpath(emailTextInputXpath)).val(incorrectEMail);
        $(By.id(okButtonId)).click();

    }

    @Then("User can see error message")
    public void userCanSeeErrorMessage() {
        $(By.id(idErrorMessage)).shouldHave(exactTextCaseSensitive(errorMessage));
    }
}
