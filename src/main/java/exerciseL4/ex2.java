package exerciseL4;

import data.Months;

import java.time.Month;
import java.util.*;

public class ex2 {
    public static void main(String[] args) {
        ArrayList<Months> aList = new ArrayList(Arrays.asList(Months.values()));
        LinkedList<Months> lList = new LinkedList(Arrays.asList(Months.JANUARY));
        HashMap<Integer, Months> hMap = new HashMap<Integer, Months>();

        System.out.println("---------------------- " + aList.getClass());
        System.out.println("initial list- - " + aList.toString());
        System.out.println("add element - " + aList.add(Months.JUNE));
        System.out.println("get element - " + aList.get(aList.size() - 1));
        System.out.println("remove element - " + aList.remove(0));
        System.out.println("final list - " + aList.toString());

        System.out.println("\n");
        System.out.println("---------------------- " + lList.getClass());
        System.out.println("initial list- - " + lList.toString());
        System.out.println("add element - " + lList.add(Months.JUNE));
        System.out.println("get element - " + lList.get(lList.size() - 1));
        System.out.println("remove element - " + lList.remove(0));
        System.out.println("final list - " + lList.toString());

        System.out.println("\n");
        System.out.println("---------------------- " + hMap.getClass());

        int i = 0;
        for (Months month : Arrays.asList(Months.values())) {
            hMap.put(++i, month);
        }
        System.out.println("initial map- - " + hMap.toString());
        System.out.println("add element - " + hMap.put(hMap.size() + 1, Months.JUNE));
        System.out.println("get element - " + hMap.get(aList.size() - 1));
        System.out.println("remove element - " + hMap.remove(1));
        System.out.println("final map - " + hMap.toString());
    }
}