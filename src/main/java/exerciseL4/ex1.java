package exerciseL4;

import data.Months;
import java.util.Locale;

public class ex1 {
    public static void main(String[] args) {
        ex1 sample = new ex1();
        System.out.println(sample.checkData("JANUARY"));
        System.out.println(sample.checkData("FEBRUary"));
        System.out.println(sample.checkData("May"));
        System.out.println(sample.checkData("september"));
        System.out.println(sample.checkData("DEZEMBER"));
    }

    public String checkData(String month) {
        try {
            return Months.valueOf(month.toUpperCase(Locale.ROOT)).getName();
        } catch (Exception e) {
            return "false";
        }
    }
}
