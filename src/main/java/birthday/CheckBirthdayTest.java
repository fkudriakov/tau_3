package birthday;

import calendar.Calendar;
import data.Months;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckBirthdayTest {
    @Test
    public void checkBirthday(){
        Calendar.checkBirthday(Months.JANUARY);

        Assert.assertTrue(Calendar.checkBirthday(Months.JANUARY).contains("winter"), "not winter");
    }
}
