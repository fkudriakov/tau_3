package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected WebDriver webDriver;
    protected WebDriverWait webDriverWait;
    private static String langLinkXpath = "//a[@id='main_menu_current_language_link']";
    private static String webexUrl = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag";
    private static String userDropdownXpath = "//a[@class='avatar_name']";
    private static String signoutButtonXpath = "//a[@id='dashboard_nav_logout_item']";
    private static String languageDropdownXpath = "//input[@id='perferenceGeneralPane-language-select']";
    private static String deStringLangXpath = "//li/span[text()='Deutsch']";
    private static String enStringLangXpath = "//li/span[text()='English']";
    private static String buttonSaveXpath = "//button[@id='preference_general_save_button']";
    private static String iconHomeXpath = "//i[@class='icon-ng-home']";

    @BeforeMethod
    public void init() {
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        webDriver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        webDriverWait = new WebDriverWait(webDriver, 20);
        openWebex();
    }

    @AfterMethod
    public void stop() {
        webDriver.quit();
    }

    private void openWebex() {
        webDriver.get(webexUrl);
        checkUrl(webexUrl);
    }

    protected void checkUrl(String url) {
        Assert.assertEquals(webDriver.getCurrentUrl(), url, "Url is not equals. ");
    }

    protected void changeLanguageTo(String expectedLang) {
        WebElement langLinkText = webDriver.findElement(By.xpath(langLinkXpath));
        langLinkText.click();
//        WebElement langDropdown = webDriver.findElement(By.xpath(languageDropdownXpath));
        webDriver.findElement(By.xpath(languageDropdownXpath)).click();
        langLinkText = webDriver.findElement(By.xpath(langLinkXpath));
        if (!langLinkText.getText().equals(expectedLang)) {
            WebElement languageText = webDriver.findElement(By.xpath(deStringLangXpath));

            if (expectedLang.equals("English")) {
                languageText = webDriver.findElement(By.xpath(enStringLangXpath));
            }

            languageText.click();
            webDriver.findElement(By.xpath(buttonSaveXpath)).click();
            webDriver.findElement(By.xpath(iconHomeXpath)).click();
        }
    }

    protected void checkLanguage(String expectedLang) {
        WebElement langLinkText = webDriver.findElement(By.xpath(langLinkXpath));
        Assert.assertEquals(langLinkText.getText(), expectedLang, "Language is incorrect. ");
    }

    protected void logout() {
        webDriver.findElement(By.xpath(userDropdownXpath)).click();
        webDriver.findElement(By.xpath(signoutButtonXpath)).click();
    }
}