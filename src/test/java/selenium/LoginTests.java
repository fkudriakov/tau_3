package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.PropertyLoader;

public class LoginTests extends BaseTest {
    private static String errorMessage = "Enter a valid email address. Example: name@email.com";
    private static String incorrectEMail = "123";
    //    private static String incorrectPassword = "Qwert123";
    private static String loginButtonId = "guest_signin_button";
    private static String emailTextInputXpath = "//input[@class='md-input']";
    private static String okButtonId = "IDButton2";
    private static String idErrorMessage = "nameContextualError1";

    private static String myPortalElemXpath = "//div[@class='idpDescription float']/span[text()='MyPortal Account (Alternative)']";
    private static String myPortalUsernameXpath = "//input[@id='username']";
    private static String myPortalPasswordXpath = "//input[@id='password']";
    private static String myPortalLoginButtonLocator = "#submit";
    private static String urlForLoggedUser = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag&from_login=true";
    private static String userDropdownXpath = "//a[@class='avatar_name']";
    private static String signoutButtonXpath = "//a[text()='Sign out']";

    private static String correctEMail = PropertyLoader.loadProperty("email");
    private static String correctPassword = PropertyLoader.loadProperty("pass");
    // ToDO: Delete pass

    @Test
    public void unsuccesfulLoginWebex() {
        checkLanguage("English");
        WebElement loginButton = webDriver.findElement(By.id(loginButtonId));
        loginButton.click();
        WebElement emailTextInput = webDriver.findElement(By.xpath(emailTextInputXpath));
        emailTextInput.sendKeys(incorrectEMail);
        WebElement okButton = webDriver.findElement(By.id(okButtonId));
        okButton.click();

        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id(idErrorMessage)));
        WebElement errorMessageElement = webDriver.findElement(By.id(idErrorMessage));
        Assert.assertEquals(errorMessageElement.getText(), errorMessage, "Error message contains unexpected text. ");
    }

    @Test
    public void successfulLoginWebex() {
        checkLanguage("English");
        WebElement loginButton = webDriver.findElement(By.id(loginButtonId));
        loginButton.click();
        WebElement emailTextInput = webDriver.findElement(By.xpath(emailTextInputXpath));
        emailTextInput.sendKeys(correctEMail);
        WebElement okButton = webDriver.findElement(By.id(okButtonId));
        okButton.click();

        WebElement myPortalElem = webDriver.findElement(By.xpath(myPortalElemXpath));
        myPortalElem.click();

        webDriver.findElement(By.xpath(myPortalUsernameXpath)).sendKeys(correctEMail);
        webDriver.findElement(By.xpath(myPortalPasswordXpath)).sendKeys(correctPassword);
        webDriver.findElement(By.cssSelector(myPortalLoginButtonLocator)).click();
        checkUrl(urlForLoggedUser);
        logout();
    }

    @Test
    public void changeLanguageTest() {
        checkLanguage("English");
        WebElement loginButton = webDriver.findElement(By.id(loginButtonId));
        loginButton.click();
        WebElement emailTextInput = webDriver.findElement(By.xpath(emailTextInputXpath));
        emailTextInput.sendKeys(correctEMail);
        WebElement okButton = webDriver.findElement(By.id(okButtonId));
        okButton.click();

        WebElement myPortalElem = webDriver.findElement(By.xpath(myPortalElemXpath));
        myPortalElem.click();

        webDriver.findElement(By.xpath(myPortalUsernameXpath)).sendKeys(correctEMail);
        webDriver.findElement(By.xpath(myPortalPasswordXpath)).sendKeys(correctPassword);
        webDriver.findElement(By.cssSelector(myPortalLoginButtonLocator)).click();

        changeLanguageTo("Deutsch");            // English

        logout();
    }
}