package selenide;

import org.openqa.selenium.By;
import org.testng.annotations.*;
import utils.PropertyLoader;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class LoginTests extends BaseTest {
    private static String errorMessage = "Enter a valid email address. Example: name@email.com";
    private static String incorrectEMail = "123";
    //    private static String incorrectPassword = "Qwert123";
    private static String loginButtonId = "guest_signin_button";
    private static String emailTextInputXpath = "//input[@class='md-input']";
    private static String okButtonId = "IDButton2";
    private static String okButtonCss = "#IDButton2";
    private static String idErrorMessage = "nameContextualError1";

    private static String myPortalElemXpath = "//div[@class='idpDescription float']/span[text()='MyPortal Account (Alternative)']";
    private static String myPortalUsernameXpath = "//input[@id='username']";
    private static String myPortalPasswordXpath = "//input[@id='password']";
    private static String myPortalLoginButtonLocator = "#submit";
    private static String urlForLoggedUser = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag&from_login=true";
    private static String userDropdownXpath = "//a[@class='avatar_name']";
    private static String signoutButtonXpath = "//a[text()='Sign out']";

    private static String correctEMail = PropertyLoader.loadProperty("email");
    private static String correctPassword = PropertyLoader.loadProperty("pass");
//    private static String myPortalLoginError = "The CIAM user id does not match valid notation. Please correct your input. (B023)";
    // ToDO: Delete pass

    @Test
    public void loginWebex() {
        $(By.id(loginButtonId)).click();
        $(By.xpath(emailTextInputXpath)).val(incorrectEMail);
        $(By.id(okButtonId)).click();
        $(By.id(idErrorMessage)).shouldHave(exactTextCaseSensitive(errorMessage));
    }

    @Test
    public void successfulLoginWebex() {

        $(By.id(loginButtonId)).click();
        $(By.xpath(emailTextInputXpath)).val(correctEMail);
        $(okButtonCss).click();
        $$(By.id(idErrorMessage)).shouldHaveSize(0);
        $(By.xpath("//span[contains(text(),'MyPortal Account')]/ancestor::div[contains(@class, 'idpDescription float')]")).click();

        $(By.name("userId")).val(correctEMail);
        $(By.id("password")).val(correctPassword);
        $(By.cssSelector(myPortalLoginButtonLocator)).click();

        $("#_loginForm_ > div.info-box.error.offset-top-3").should(not(exist));
        $$(By.xpath("//*[@class='pmr_card_c_title']/h1")).texts().contains("Personal Room");
//        Assert.assertFalse(url().contains("home"), "Url is not home. " + url());

        $$(By.xpath("//tbody/tr")).shouldHaveSize(4);
        $$(By.xpath("//tbody/tr//div[@class='meeting_topic home']/a")).filterBy(text("TAU")).shouldHaveSize(1);
        $$(By.xpath("//tbody/tr//div[@class='meeting_topic home']/a")).filterBy(text("Daily"));

        $$(By.xpath("//ul[@class='nav_tabs']/li")).shouldHaveSize(8);
        $$(By.xpath("//ul[@class='nav_links']/li")).shouldHaveSize(3);

        $(By.xpath(userDropdownXpath)).click();
        $(By.xpath(signoutButtonXpath)).click();
//        $("#success").should(exist).text().trim().compareToIgnoreCase("WebEx Sign Out successful. Close browser to sign out of other services");
    }
}