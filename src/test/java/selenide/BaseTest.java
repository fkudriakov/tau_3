package selenide;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.*;
import utils.PropertyLoader;

import static com.codeborne.selenide.Selenide.open;

public class BaseTest {

    private String webexUrl = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag";

    @BeforeMethod
    public void init() {
        Configuration.browser = PropertyLoader.loadProperty("browser.name");
//        Configuration.browserCapabilities.setCapability("headless",true);
//        Configuration.headless = true;
        open(webexUrl);
    }

}
