Feature: Check login

  Scenario: login is successful
    Given Webex start page is opened
    When Data of sign in page is submitted
    And Login data of portal is submitted
    Then User is logged successful
    And User logout

  Scenario: login is unsuccessful
    Given Webex start page is opened
    When Login data is incorrect
    Then User can see error message