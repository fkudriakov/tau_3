package cucumber;

import io.cucumber.java.en.*;
import org.junit.Assert;
import utils.jira_xray.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class JiraStepdefs {
    List<String> ids = new ArrayList<>();
    File outputDir;

    @Given("feature fetch parameters are defined")
    public void featureFetchParametersAreDefined() {
        ids.add("TAAADEV-570");
        outputDir = new File("src/test/java/resources/jira");
    }

    @Given("feature fetch parameters are {string}")
    public void featureFetchParametersAre(String casesList) {
        String[] splitCases = casesList.split("[ ,]+");
        for (String caseName : splitCases) {
            ids.add(caseName.trim());
        }
        outputDir = new File("src/test/java/resources/jira");
    }

    @When("a request to jira xray is sent")
    public void aRequestToJiraXrayIsSent() {
        XRayCucumberFeatureFetcher featureFetcher = new XRayCucumberFeatureFetcher();
        featureFetcher.fetch(ids, outputDir);
    }

    @Then("feature file is received")
    public void featureFileIsReceived() throws IOException {
        Assert.assertTrue("No files received", Files.list(Paths.get("src/test/java/resources/jira")).findAny().isPresent());
    }

    @Given("test result file is present")
    public void testResultFileIsPresent() throws IOException {
        Assert.assertTrue("No files received", Files.list(Paths.get("target/cucumber-html-reports")).findAny().isPresent());
    }

    @When("test result file to jira xray is sent")
    public void testResultFileToJiraXrayIsSent() {
        File resultsFile = new File("target/cucumber-html-reports/cucumber.json");
        XRayCucumberResultsImporter resultsImporter = new XRayCucumberResultsImporter();
        resultsImporter.importResults(resultsFile);
    }

    @Then("results are successfully imported")
    public void resultsAreSuccessfullyImported() {
    }
}