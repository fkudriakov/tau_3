package cucumber;

import io.cucumber.junit.*;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/resources/jira",
        plugin = {"json:target/cucumber-html-reports/cucumber.json", "pretty"}
)
public class GoogleTest {
}
