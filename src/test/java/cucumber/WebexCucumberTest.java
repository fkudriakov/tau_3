package cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.*;

@RunWith(Cucumber.class)
@CucumberOptions(
//        features = "src/test/java/resources/webex_login.feature",
        features = "src/test/java/resources/jira",
        plugin = {"json:target/cucumber-html-reports/cucumber.json", "pretty"}
)
public class WebexCucumberTest {
}
