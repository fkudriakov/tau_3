package cucumber;

import io.cucumber.junit.*;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/resources/get_features_jira_xray.feature",
        plugin = {"pretty"}
)

public class JiraExportTest {
}
