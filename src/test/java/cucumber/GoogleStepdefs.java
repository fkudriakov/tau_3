package cucumber;

import com.codeborne.selenide.Condition;
import io.cucumber.java.en.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class GoogleStepdefs {
    private static final String APP = "https://www.google.com";
    private final By SEARCH_FIELD_LOCATOR = By.xpath("//input[@name='q']");
//    private final By SEARCH_BUTTON_LOCATOR = By.xpath("//div[@class='FPdoLc tfB0Bf']//input[@name='btnK']");
    private final By SEARCH_BUTTON_LOCATOR = By.xpath("//input[@name='btnK']");
    private final By ALL_TAB_LOCATOR = By.xpath("//div[@class='hdtb-mitem hdtb-msel']");

    @Given("^google main page is opened$")
    public void googleMainPageIsOpened() {
        open(APP);
    }

    @When("^search some data$")
    public void searchSomeData() {
        $(SEARCH_FIELD_LOCATOR).val("TAU");
        $(SEARCH_BUTTON_LOCATOR).click();
    }

    @Then("^tab will results is displayed$")
    public void tabWillResultsIsDisplayed() {
        $(ALL_TAB_LOCATOR).shouldBe(Condition.exist);
    }

    @Given("search is done")
    public void searchDone(){
        googleMainPageIsOpened();
        searchSomeData();
        tabWillResultsIsDisplayed();
    }
}
