Feature:  Fetch features from Jira Xray

  Scenario: Successful feature fetch
    Given feature fetch parameters are defined
    When a request to jira xray is sent
    Then feature file is received