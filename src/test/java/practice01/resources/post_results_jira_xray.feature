Feature:  Import features to Jira Xray

  Scenario: Successful feature result import
    Given test result file is present
    When test result file to jira xray is sent
    Then results are successfully imported