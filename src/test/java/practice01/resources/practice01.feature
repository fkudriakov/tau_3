Feature: Create meeting on webex

  Scenario: Successfully created meeting
    Given User logged
    When User create meeting
    And User typed correct meeting data
    Then User see page of success and validate data
    And User logouts