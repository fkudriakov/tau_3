package practice01.cucumber;

import io.cucumber.junit.*;
import org.junit.runner.*;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/practice01/resources/jira",
        plugin = {"json:target/cucumber-html-reports/cucumber.json", "pretty"}
)
public class WebexPr1Test {
}
