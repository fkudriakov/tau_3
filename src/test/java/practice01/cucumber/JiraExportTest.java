package practice01.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/practice01/resources/get_features_jira_xray.feature",
        plugin = {"pretty"}
)

public class JiraExportTest {
}
