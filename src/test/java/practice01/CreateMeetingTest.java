package practice01;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.annotations.*;
import utils.PropertyLoader;

import java.time.LocalDate;
import java.util.Locale;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class CreateMeetingTest extends BaseTest {
    private static String errorMessage = "Enter a valid email address. Example: name@email.com";
    private static String incorrectEMail = "123";
    //    private static String incorrectPassword = "Qwert123";
    private static String loginButtonId = "guest_signin_button";
    private static String emailTextInputXpath = "//input[@class='md-input']";
    private static String okButtonId = "IDButton2";
    private static String okButtonCss = "#IDButton2";
    private static String idErrorMessage = "nameContextualError1";

    private static String myPortalElemXpath = "//div[@class='idpDescription float']/span[text()='MyPortal Account (Alternative)']";
    private static String myPortalUsernameXpath = "//input[@id='username']";
    private static String myPortalPasswordXpath = "//input[@id='password']";
    private static String myPortalLoginButtonLocator = "#submit";
    private static String urlForLoggedUser = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag&from_login=true";
    private static String userDropdownXpath = "//a[@class='avatar_name']";
    private static String signoutButtonXpath = "//a[text()='Sign out']";

    private static String correctEMail = PropertyLoader.loadProperty("email");
    private static String correctPassword = PropertyLoader.loadProperty("pass");
//    private static String myPortalLoginError = "The CIAM user id does not match valid notation. Please correct your input. (B023)";
    // ToDO: Delete pass


    @Test
    public void createWebexMeetingTest() {

        $(By.id(loginButtonId)).click();
        $(By.xpath(emailTextInputXpath)).val(correctEMail);
        $(okButtonCss).click();
        $$(By.id(idErrorMessage)).shouldHaveSize(0);
        $(By.xpath("//span[contains(text(),'MyPortal Account')]/ancestor::div[contains(@class, 'idpDescription float')]")).click();

        $(By.name("userId")).val(correctEMail);
        $(By.id("password")).val(correctPassword);
        $(By.cssSelector(myPortalLoginButtonLocator)).click();

        $("#_loginForm_ > div.info-box.error.offset-top-3").should(not(exist));
        $$(By.xpath("//*[@class='pmr_card_c_title']/h1")).texts().contains("Personal Room");

        final By MEETINGS_BUTTON_LOCATOR = By.xpath("//a[@id='dashboard_nav_meeting_item']");
        $(MEETINGS_BUTTON_LOCATOR).click();
        // check page
        Assert.assertTrue("", $("div.fixed-header.host > h1").getText().toString().toLowerCase(Locale.ROOT).equals("my webex meetings"));

        final By SCHEDULE_BUTTON_LOCATOR = By.xpath("//button[@id='scheduleEntry']");
        $(SCHEDULE_BUTTON_LOCATOR).click();
        //check
        Assert.assertTrue("", $("h1.grid_block_t.module_header.schedule-title").getText().toString().toLowerCase(Locale.ROOT).equals("schedule a meeting"));

        final By MEETING_TOPIC_INPUT_LOCATOR = By.xpath("//input[@id='topic']");
        $(MEETING_TOPIC_INPUT_LOCATOR).sendKeys(Keys.CONTROL + "a");
        $(MEETING_TOPIC_INPUT_LOCATOR).sendKeys(Keys.BACK_SPACE);
        final String NAME_FOR_MEETING_STR = "Meeting practise01";
        $(MEETING_TOPIC_INPUT_LOCATOR).setValue(NAME_FOR_MEETING_STR).pressEnter();


        final By SCHEDULE_TIME_DOWNDROP_LOCATOR = By.xpath("//a[@id='scheduleTimeTrigger']/i[@class='icon-arrow']");
        $(SCHEDULE_TIME_DOWNDROP_LOCATOR).click();


        final By TODAY_DATE_LOCATOR = By.cssSelector("td.available.today");

        String data = $(TODAY_DATE_LOCATOR).getText();
        int i = Integer.parseInt(data);

        LocalDate now = LocalDate.now();
        // check date Assert.true("", now.getDayOfMonth().equals(i));
        int nextDay = now.plusDays(1).getDayOfMonth();

        String template = "//td[@class='available'or @class='next-month']//span[text()=%d]";
        String xpath = String.format(template, nextDay);
        $(By.xpath(xpath)).click();

        final By DONE_BUTTON_IN_DATE_LOCATOR = By.xpath("//button[@id='scheduleDone']");
        $(DONE_BUTTON_IN_DATE_LOCATOR).click();

        final String emailParticipants = "Elena.Sergeeva@t-systems.en";
        //todo change .en to .com
//        final String emailParticipants= "Elena.Sergeeva@t-systems.com";
        final By PARTICIPANTS_INPUT_LOCATOR = By.xpath("//input[@name='predictivesearch']");
        $(PARTICIPANTS_INPUT_LOCATOR).sendKeys(emailParticipants);
        $(PARTICIPANTS_INPUT_LOCATOR).sendKeys(Keys.TAB);
        final By START_BUTTON_LOCATOR = By.xpath("//button[@id='scheduleEdit']");
        $(START_BUTTON_LOCATOR).click();


        Assert.assertTrue("", $(By.xpath("//div[@class='meeting_topic_wrapper']/h1")).getText().toString().toLowerCase(Locale.ROOT).equals(NAME_FOR_MEETING_STR.toLowerCase(Locale.ROOT)));

        Assert.assertTrue("", $(By.xpath("//div[@class='mdp_attendee_item']/div")).getText().toString().toLowerCase(Locale.ROOT).equals(emailParticipants.toLowerCase(Locale.ROOT)));

        $(By.xpath(userDropdownXpath)).click();
        $(By.xpath(signoutButtonXpath)).click();
//        $("#success").should(exist).text().trim().compareToIgnoreCase("WebEx Sign Out successful. Close browser to sign out of other services");
    }
}